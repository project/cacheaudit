README
======

This Drush plugin provides a Drush command to quickly review cache settings. Currently supports the following modules -

  - Drupal core
  - Block (Drupal core)
  - Views

